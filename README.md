# Jelp Practice Git

Este repositorio es para poder practicar temas de GIT.

## Recursos

* [Version Control Best Practices](https://www.git-tower.com/learn/git/ebook/en/command-line/appendix/best-practices)
* [Become a Professional - Git Tower](https://www.git-tower.com/learn/)
* [Learn Git Branching](http://learngitbranching.js.org/)
* [Got 15 minutes and want to learn Git? - Github](https://try.github.io/levels/1/challenges/1)
* [Learn Git - Code Academy](https://www.codecademy.com/learn/learn-git)
* [Comparin Git Workflows - Atlassian](https://www.atlassian.com/git/tutorials/comparing-workflows)

## Git Clients

* [Git](https://git-scm.com/)
* [Source Tree](https://www.sourcetreeapp.com/)
* [GitKraken](https://www.gitkraken.com/)
* [Gitup](http://gitup.co/)
* [Git Tower](https://www.git-tower.com/)

## Contributors

* [Josuebasurto](http://twitter.com/josuebasurto)